<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Code&Text Similarity</title>
        <link rel="shortcut icon" type="images/x-icon" href="./fav-icon.PNG">
        <meta name="keywords" content="Code Similarity,text similarity,text,code,C#,Java,python">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Css tanımlamaları-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" >
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="CodeSimilarity.css">
       
        <!-- Javascript Tanımlamaları-->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
        <script src="./CodeSimilarity.js" ></script>

        <style>
        .loading-page {
    width:100%;
    height:90vh;
    margin-top:-10vh;
    margin-left:-5vh;
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    display: flex;
}

.loading-page .counter {
    text-align: center;
}

.loading-page .counter p {
    font-size: 4vh;
    font-weight:bold;
    color: #064579;
    margin: 0;
    padding: 0;
}

.loading-page .counter h1 {
    color: white;
    font-size:  12vh;
    margin-top: 2vh;
    margin: 0;
    padding: 0;
    color: #dfdfaf;
}

.loading-page .counter hr {
    background: #064579;
    border: none;
    height: 1vh;
}

.loading-page .counter {
    position: relative;
    width: 45%;
}

.loading-page .counter h1.abs {
    position: absolute;
    top: 0;
    width: 100%;
}

.loading-page .counter .color {
    width: 0px;
    overflow: hidden;
    color: #064579;
}
::-webkit-scrollbar {
  width: 1.5vh;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 4px black; 
  border-radius: 5vh;
  margin:0px;
  padding:0px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: red; 
  border-radius: 25vh;
  background-color:gray;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background-color: gray; 
}
    </style>
    </head>
    <body>
        <div class="container-fluid">
            <!-- Top Menü -->
            <nav class="navbar">
            <img src="./logo.PNG">
            </nav>
        <!-- End Top Menü -->
            <div class="row">
                <div class="col-2">
                    <div class="sidenav">
                    <ul id="leftMenu" >
                        <li id="textS" > <a href="./TextSimilarity.php">Text Similarity</a></li>
                        <li id="codeS" class="active"> <a href="./CodeSimilarity.php">Code Similarity</a></li>
                        <li id="contact"><a href="./Contact.php">Application Guide</a></li>
                    </ul>
                    </div>
                </div>
                <div class="col-10">
                    <div id="homeDiv">
                        <?php

                        ini_set('max_execution_time', 0);

                        $dir = "upload/*";
                        $files = glob($dir);
                        foreach ($files as $file) :
                            unlink($file);
                        endforeach;
                     
                        if(!isset($_POST['submit'])) {?>
                        <div class="container">
                        
                            <div class="text-center" style="display:block;">
                             
                              <!-- Test all icons -->
                              <h1 style="margin-bottom: 5vh; padding-top: 15vh; font-size:10vh;">
                                <i class="far fa-file-code text-info"></i>
                           
                              </h1>
                                <!-- This file input will automatically converted into "Bootstrap File Input" -->
                                <!-- Iconic preview for thumbs and detailed preview for zoom -->
                                <form action="" method="post" enctype="multipart/form-data">
                                <div class="file-loading">
                                  <input id="input-ficons-5" name="file[]" multiple type="file" accept=".java , .html , .php , .js, .py, .c++, .cpp, .c, .cs, .css, .m">
                                </div>
                                <p>&nbsp;</p>
                                <button type="submit" class="btn btn-default" name='submit' style="font-weight: bold; background-color:#064579; color:#dfdfaf;" >Upload</button>
                                </form>
                            </div>
                          </div>
                       <?php }else{ ?> 
                        <div id="loader" style="width:100%; height:90vh; display:block;">
                            <div class="loading-page">
                                <div class="counter">
                                    <p>LOADING SIMILARITY..</p>
                                    <h1>0%
                                    <!--h1.abs loading h1.abs.color loading-->
                                    </h1>
                                    <hr />
                                </div>
                            </div>
                        </div>
                        <div id="tableScene" style="display:none;">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>First</th>
                                <th>Second</th>
                                <th>Similarity Rate</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                        <tbody>
                       <?php
                        if (isset($_POST['submit'])) {
                            // Count total files
                            $countfiles = count($_FILES['file']['name']);
                        
                            // Looping all files
                            for ($i = 0; $i < $countfiles; $i++) {
                                $filename = $_FILES['file']['name'][$i];
                                // Upload file
                                move_uploaded_file($_FILES['file']['tmp_name'][$i], 'upload/' . $filename.'.'."txt");
                                    
                            }

                            ?>
                                    <script>

                                    function loaderBar(degisken){
                                       <?php
                                      $sayac = $countfiles; 
                                      
                                      $sayac = ((($sayac-1)*$sayac)/2); ?>
                                     
                                      var tableScene = document.getElementById("tableScene"); 
                                      var loader = document.getElementById("loader");

                                      var sayac= <?php echo $sayac; ?>;
                                      var c = (100/sayac)*degisken;
                                      
                                          $(".loading-page .counter h1").html(c + "%");
                                          $(".loading-page .counter hr").css("width", c + "%");
                                        if(c == 100) {
                                            loader.style.display="none";
                                            tableScene.style.display="block";
                                        }
                                    }
                                    </script>
                                    <script>

                                    function loaderBar(degisken){
                                       <?php
                                      $sayac = $countfiles; 
                                      
                                      $sayac = ((($sayac-1)*$sayac)/2); ?>
                                     
                                      var tableScene = document.getElementById("tableScene"); 
                                      var loader = document.getElementById("loader");

                                      var sayac= <?php echo $sayac; ?>;
                                      var c = (100/sayac)*degisken;
                                      
                                          $(".loading-page .counter h1").html(c + "%");
                                          $(".loading-page .counter hr").css("width", c + "%");
                                        if(c == 100) {
                                            loader.style.display="none";
                                            tableScene.style.display="block";
                                        }
                                    }
                                    </script> <?php
                        
                        
                        
                            $dir = "upload/*";
                            $files = glob($dir);
                            $filenames = array();
                            $termsDocsArray = array();
                            $allTerms = array();
                            $tfidfDocsVector = array();
                        
                            foreach ($files as $file) :
                                array_push($filenames, basename($file));
                                $lineList = array();
                                $myfile = fopen($file, "r") or die("Unable to open file!");
                        
                                while (!feof($myfile)) {
                                    $line = trim(fgets($myfile));
                                    if(!empty($line) && $line != "}" && $line != "}")
                                    array_push($lineList, $line);
                                }
                                
                                foreach ($lineList as $term) {
                                    if (!in_array($term, $allTerms)) {
                                        array_push($allTerms, $term);
                                    }
                                }
                                array_push($termsDocsArray, $lineList);
                                fclose($myfile);
                            endforeach;
                            
                        
                        
                            function tfCalculator($totalterms, $termToCheck)
                            {
                                $count = 0;
                                foreach ($totalterms as $s) {
                                    if (!strcasecmp($s, $termToCheck)) {
                                        $count++;
                                    }
                                }
                                $sonuc = $count / count($totalterms);
                              
                                return $sonuc;
                            }
                        
                        
                            function idfCalculator($allTerms, $termToCheck)
                            {
                               
                                $count = 0;
                                foreach ($allTerms as $ss) {
                                    foreach ($ss as $s) {
                                        if (!strcasecmp($s, $termToCheck)) {
                                            $count++;
                                            break;
                                        }
                                    }
                                }
                                $sonuc = 1 + log(count($allTerms) / $count);
                         
                                return $sonuc;
                            }
                        
                        
                            function cosineSimilarity($docVector1, $docVector2)
                            {
                                $dotProduct = 0.0;
                                $magnitude1 = 0.0;
                                $magnitude2 = 0.0;
                                $cosineSimilarity = 0.0;
                                  //  echo count($docVector1)."test";
                                for ($i = 0; $i < count($docVector1); $i++) {
                                    $dotProduct += $docVector1[$i] * $docVector2[$i];  //a.b
                                    $magnitude1 += pow($docVector1[$i], 2);  //(a^2)
                                    $magnitude2 += pow($docVector2[$i], 2); //(b^2)
                                  //  echo $magnitude1."bu bşrrr".$magnitude2."bu ikii"."<br>";
                                }
                              
                                $magnitude1 = sqrt($magnitude1); //sqrt(a^2)
                                $magnitude2 = sqrt($magnitude2); //sqrt(b^2)
                                
                                if ($magnitude1 != 0.0 | $magnitude2 != 0.0) {
                                    $cosineSimilarity = $dotProduct / ($magnitude1 * $magnitude2);
                                 //   echo "Girdiii vallaaa";
                                } else {
                                    return 0.0;
                                }
                               // echo $cosineSimilarity."asdas"."<br>";
                                return $cosineSimilarity;
                            }
                        
                        
                        
                        
                            foreach ($termsDocsArray as $docTermsArray) {
                                $tfIdfVectors = array();
                                $count = 0;
                                foreach ($allTerms as $terms) {
                                    $tf = tfCalculator($docTermsArray, $terms);
                                    $idf = idfCalculator($termsDocsArray, $terms);
                                    $tfidf = $tf * $idf;
                                    array_push($tfIdfVectors, $tfidf);
                                    $count++;
                                }
                                //print_r($tfIdfVectors);
                                array_push($tfidfDocsVector, $tfIdfVectors);
                               
                            }
                            $loadingCounter=0;
                            $geciciName="";
                            for ($i = 0; $i < count($tfidfDocsVector); $i++) {

                                for ($j = $i; $j < count($tfidfDocsVector); $j++) {
                                    if ($i != $j) {
                                        $sendArrayContent = array();
                                        array_push($sendArrayContent, $filenames[$i]);
                                        array_push($sendArrayContent, $filenames[$j]);
                                        array_push($sendArrayContent, number_format(100 * (cosineSimilarity($tfidfDocsVector[$i], $tfidfDocsVector[$j]))));
                                        /*  array_push($sendArrayContent, $termsDocsArray[$i]);
                                        array_push($sendArrayContent, $termsDocsArray[$j]);
                                        array_push($sendArrayContent, $tfidfDocsVector[$i]);
                                        array_push($sendArrayContent, $tfidfDocsVector[$j]);*/
                                        $loadingCounter++;
                                        ?><script> loaderBar(<?=$loadingCounter ?>) </script><?php
                        ?>
                                        <tr style="color:white;">
                                            <td><?php  $geciciName = $filenames[$i];
                                                            $geciciName = substr_replace($geciciName, '', -4);
                                                           echo $geciciName; ?></td>
                                            <td><?php  $geciciName = $filenames[$j];
                                                    $geciciName = substr_replace($geciciName, '', -4);
                                                   echo $geciciName; ?></td>

                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar bg-info" style="width:<?php echo (100 * (cosineSimilarity($tfidfDocsVector[$i], $tfidfDocsVector[$j]))) ?>%; background-color:#064579;">
                                                        <?php echo (number_format(100 * (cosineSimilarity($tfidfDocsVector[$i], $tfidfDocsVector[$j])))) ?>
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <form method="get" action="CodeDetails.php">
                                                        <a href="CodeDetails.php?fileOne=<?= $sendArrayContent[0] ?>&fileTwo=<?= $sendArrayContent[1] ?>&similarity=<?= $sendArrayContent[2] ?>" target="_blank" style="font-weight:bold;"> Details → </a>
                                       
                                                </form>
                                            </td>
                                        </tr>
                        <?php

                                    }
                                }
                            }
                        }
                        ?> </tbody>
                    <tfoot>
                        <tr>
                            <th>First</th>
                            <th>Second</th>
                            <th>Similarity Rate</th>
                            <th>Details</th>
                        </tr>
                    </tfoot>
                </table> </div><?php

                        }
                            ?>

        </div>
    </div>
</div>
</div>
</body>

</html>
    