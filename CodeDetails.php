<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Code&Text Similarity</title>
<link rel="shortcut icon" type="images/x-icon" href="./fav-icon.PNG">
    <meta name="keywords" content="Code Similarity,text similarity,text,code,C#,Java,python">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Css tanımlamaları-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="CodeSimilarity.css">
    <link rel="stylesheet" type="text/css" href="TextDetails.css">
    <!-- Javascript Tanımlamaları-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<?php

if (isset($_GET["fileOne"]) && isset($_GET["fileTwo"]) && isset($_GET["similarity"])) {
    $fileOneName = $_GET["fileOne"];
    $fileTwoName = $_GET["fileTwo"];
    $similarity = $_GET["similarity"];
    $dir = "upload/*";
    $files = glob($dir);
    $termsDocsArray = array();
    foreach ($files as $file) :
        if (basename($file) == $fileOneName || basename($file) == $fileTwoName) {
            $lineList = array();
            $myfile = fopen($file, "r") or die("Unable to open file!");
    
            while (!feof($myfile)) {

                $line = trim(fgets($myfile));
                if(!empty($line))
                array_push($lineList, $line);
            }
            
            array_push($termsDocsArray, $lineList);
            fclose($myfile);
        }
    endforeach;

    $firstTermArray = $termsDocsArray[0];
    $secondTermArray = $termsDocsArray[1];
    $count=0;
    $count2=0;
    foreach ($firstTermArray as $term) {
        if($term != "{" && $term != "}"){
            if (in_array($term, $secondTermArray)) {
                $count++;
            } else {
            }
        }
    }
    foreach ($secondTermArray as $term_2) {
        if($term_2 != "{" || $term_2 != "}"){
            if (in_array($term_2, $firstTermArray)) {
                $count2++;
            } else {
            }
        }    
    }
?>
<style>
xmp{
        white-space: pre-wrap;
        word-wrap: normal; 
    }
    ::-webkit-scrollbar {
  width: 1.5vh;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 4px gray; 
  border-radius: 5vh;
  margin:0px;
  padding:0px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: red; 
  border-radius: 25vh;
  background-color:gray;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background-color: gray; 
}
</style>
</head> 
<body>
    <div class="container-fluid">
        <!-- Top Menü -->
        <nav class="navbar">
                <img src="./logo.PNG">
        </nav>
        <div class="topMenu">
            <div class="row">
                <div class="col-2">
                    <div class="card-box bg-blue">
                        <div class="inner">
                            <h3> <?php echo count($secondTermArray);?> </h3>
                            <p id="CardBody"> <?php 
                             $fileOneName = substr_replace($fileOneName, '', -4);
                            echo $fileOneName." Total Lines";?>  </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-2">
                     <div class="card-box bg-blue">
                        <div class="inner">
                            <h3> <?php echo count($firstTermArray);?> </h3>
                            <p id="CardBody"><?php
                             $fileTwoName = substr_replace($fileTwoName, '', -4);
                             echo $fileTwoName." Total Lines";?> </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-2">
                    <div class="card-box bg-blue">
                        <div class="inner">
                            <h3> <?php if($count < $count2 )
                                            echo $count;
                                        else 
                                            echo $count2 ?> </h3>
                            <p id="CardBody"> Similar Lines </p>
                        </div>
                    </div>
                </div>

                <div class="col-2">
                    <div class="card-box bg-blue">
                        <div class="inner">
                            <h3> <?php echo $similarity."%";?> </h3>
                            <p id="CardBody"> Similarity </p>
                        </div>
                    </div>
                </div>

                <div class="col-2" >
                    <table >
                            <tr > <p style="margin-top:1.2vh; margin-bottom:1.2vh;"> Similarity : </p></tr>
                            <tr > <div class="progress" style="width:95%;">
                                <div class="progress-bar " style="width:<?php echo $similarity ?>%; background-color:red;color:#dfdfaf;">
                                <?php echo  $similarity ?>
                                </div>
                            </tr>
                           
                            <tr> <p style="margin-top:1.2vh; margin-bottom:1.2vh;"> Original :</p></tr>
                            <tr> <div class="progress" style="width:95%;">
                                <div class="progress-bar " style="width:<?php echo (100-$similarity) ?>%; background-color:#dfdfaf; color:black;">
                                <?php echo (100-$similarity) ?>
                                </div>
                            </tr>
                    </table>
                </div>
                
                <div class="col-2">
                <canvas id="myChart" width="100%" height="45%"></canvas>
                </div>

                </div>
        </div>
        <div class="row" style="margin-top:2vh;">
            <div class="col-6" style=" padding-top:1vh; padding-right:1vh; margin-bottom:4vh;" >
            <div class="card" style=" overflow-y:auto; height:80vh; ">
            <h5 class="card-title" style="text-align:center; margin-top:1vh;"> <?= $fileOneName ?></h5>
            <hr style="margin:0px; padding:0px;">
                <div class="card-body" >
                    <?php
                        $doc="";
                        foreach ($firstTermArray as $term) {
                            if (in_array($term, $secondTermArray)) {
                             $doc .="<pre style='color:red; font-weight:bold; margin:0px; padding:0px;  overflow-x: hidden; '><xmp style='margin:0px; padding:0px;'>".$term."</xmp></pre>" ;
                            } else {
                             $doc .= "<pre style='margin:0px; padding:0px; overflow-x: hidden;'><xmp style='margin:0px; padding:0px;'>".$term."</xmp></pre>" ;
                            }
                        }
                        
                        ?>
                      <?= $doc?> 
                  </div>
                </div>
            </div> 
            <div class="col-6" style=" padding-top:1vh; margin-bottom:4vh; padding-left:1vh;">
            <div class="card" style=" overflow-y:auto; height:80vh; ">
            <h5 class="card-title" style="text-align:center; margin-top:1vh;"> <?= $fileTwoName ?></h5>
            <hr style="margin:0px; padding:0px;">
                <div class="card-body" >
           <?php 
           $doc="";
           foreach ($secondTermArray as $term_2) {
                if (in_array($term_2, $firstTermArray)) {
           
                    $doc .= " <pre style='color:red; font-weight:bold; margin:0px; padding:0px; overflow-x: hidden;'><xmp style='margin:0px; padding:0px;'>".$term_2."</xmp></pre>" ;
                
                } else {
                    $doc .="  <pre style='margin:0px; padding:0px; overflow-x: hidden;'><xmp style='margin:0px; padding:0px;'>".$term_2."</xmp></pre>";
            
                }
            }
            ?>
            <?= $doc?> 
    <?php
    }
    ?> </div>
    </div>
            </div>
        </div>
    </div>
</body>
<script>
    var deger=<?php echo $similarity; ?>;
    var deger2=100-<?php echo $similarity; ?>;
 var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        datasets: [{
            backgroundColor: ["red", "#dfdfaf"],
            borderWidth:0,
        data: [deger,deger2]
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
        'Similarity',
        'Original'
    ]
    },

    // Configuration options go here
    options: { 
        legend: {
            display: false
        }
    }
});

</script>
</html>
   
