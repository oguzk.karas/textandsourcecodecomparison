<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Code&Text Similarity</title>
        <link rel="shortcut icon" type="images/x-icon" href="./fav-icon.PNG">
        <meta name="keywords" content="Code Similarity,text similarity,text,code,C#,Java,python">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Css tanımlamaları-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" >
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="CodeSimilarity.css">
       
        <!-- Javascript Tanımlamaları-->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
        <script src="./CodeSimilarity.js" ></script>

        <style>

::-webkit-scrollbar {
  width: 1.5vh;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 4px black; 
  border-radius: 5vh;
  margin:0px;
  padding:0px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: red; 
  border-radius: 25vh;
  background-color:gray;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background-color: gray; 
}
    </style>
    </head>
    <body>
        <div class="container-fluid">
            <!-- Top Menü -->
            <nav class="navbar">
            <img src="./logo.PNG">
            </nav>
        <!-- End Top Menü -->
            <div class="row">
                <div class="col-2">
                    <div class="sidenav">
                    <ul id="leftMenu" >
                        <li id="textS" > <a href="./TextSimilarity.php">Text Similarity</a></li>
                        <li id="codeS"> <a href="./CodeSimilarity.php">Code Similarity</a></li>
                        <li id="contact"  class="active"><a href="./Contact.php">Application Guide</a></li>
                    </ul>
                    </div>
                </div>
                <div class="col-10">
                    <div id="header" style="text-align:center; "> <h3>Code&Text Similarity Guide</h3>
                    <pre style="margin-top:2vh;">Code&Text Similarity  application is simple and simple to use. <br> First, decide on the type of file you want to compare.  </pre><img src="./images/secim.png" alt="">
                    <pre style="margin-top:1vh;">If the file you want to compare belongs to the text type, select the text similarity menu. <br> If the file you want to compare is any code file, select the code similarity menu. <br> After making the appropriate selection, follow the same steps for both menus.

                    </pre>
                    <img src="./images/upload.png" alt="" style="margin-top:0vh; padding:0px;">
                    <pre>Select the files you want to compare and click the upload button. <br><b style="color:red;"> You can choose up to 35 files! </b> <br><b style="color:red;"> Choose multiple files! </b> <br> The loading speed varies depending on the file size. You should wait a bit for long files. <br> You will be informed during the calculation.</pre>
                    <img src="./images/loader.png" alt="">
                    <pre> The operation is completed when the loader bar is 100%. <br> <b style="color:red;">If it is not completed, report it to us as an error.</b> <br> After the process is completed, you will see a table presenting the similarity rates.</pre>
                    <img src="./images/table.png" alt="" style="width:85%;">
                    <pre style="margin-top:2vh;"> The table contains the two file names compared, the similarity rate and the detail button. <br> Similarity rates are shown in bars. <br> Click the detail button for more details about the two documents. <br>You can also perform various filtering operations on the table.</pre>
                    <img src="./images/DetailsTop.png" alt="" style="width:85%;">
                    <pre style="margin-top:2vh;">the first two values show the total number of words in the file,<br> the third value shows the number of similar words, and the fourth value shows the similarity rate. <br> Similarity ratios are visualized with bar and pie chart. <br> Red color is similar and white color is different. <br> Along with this information, the two texts are displayed on the detail page. <br> Words that are similar are shown in red.</pre>
                    <img src="./images/DetailsP.png" alt="" style="width:70%;">
                    <pre style="margin-top:2vh;"> The filename related to the content is at the top of the title. <br> <b style="color:red;">Congratulations, you have completed the guide.</b></pre>
                    <pre style="margin-top:2vh;"><b style="color:red"> Note that this application only supports the .txt extension and code files.</b></pre>
                    <pre style="margin-top:2vh;">Let us know what errors you encountered. <br> You can submit your suggestions to improve the application. <br> </pre>
                    <pre style="margin-top:4vh;"><b style="color:red; font-size:3vh;"> Contact information: yasar.kurt46@outlook.com  oguzk.karas@gmail.com  hcocalak@gmail.com </b></pre>
       
                                 </div>
                </div>
</div>
</div>
</body>

</html>
    